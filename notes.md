WANING STARLIGHT
CORPUS ALCHEMIA


Quotes:
Aristotle stated that the most fundamental (hence
“primary”) qualities of any thing are hot, cold, wet, and dry. When pairs
of these qualities are joined to matter, the four elements—fire, air, water,
and earth—result


Features:

== MILESTONE 1 ==
+ fov
maps
effects
monsters (req. effects)
bump combat

== MILESTONE 2 ==
mana
simplified spellcasting (req. mana)
tiles (req. effects)

== MILESTONE 3 ==
modular spellcasting (req. tiles, simplified casting)

== MILESTONE 4 ==
items
alchemy (req items)


mouse support









Alchemy!


Group: A5
Order: 60

Conjugacy Classes:
(.)      : 1
(..)(..) : 15
(...)    : 20
(.....)  : 24


(x, x inv) pairs:
(...) : 10
Sol (Gold)        -- 3   -- 4
Luna (Silver)     -- 8   -- 12
Mercury (Mercury) -- 11  -- 19
Venus (Copper)    -- 15  -- 20
Mars (Iron)       -- 30  -- 48
Jupiter (Tin)     -- 38  -- 74
Saturn (Lead)     -- 45  -- 99
Uranus            -- 56  -- 78
Neptune           -- 59  -- 103
Pluto             -- 81  -- 104


(.....) : 12
12345
12354
12435
12453
12534
12543
13245
13254
13425
13452
13524
13542

Aries
Taurus
Gemini
Cancer
Leo
Virgo
Libra
Scorpius
Sagittarius
Capricornus
Aquarius
Pisces


Other (24):
1 Ceres
2 Pallas
3 Juno
4 Vesta
5 Astraea
6 Hebe
7 Iris
8 Flora
9 Metis
10 Hygiea
11 Parthenope
12 Victoria
13 Egeria
14 Irene
15 Eunomia
16 Psyche
17 Thetis
18 Melpomene
19 Fortuna
26 Proserpina
28 Bellona
29 Amphitrite
35 Leukothea
37 Fides
Earth


Other classes in S5:
(..) : 10      - Order 2
(...)(..) : 20 - Order 6
(....) : 30    - Order 4


== Combining ==

3 parts:
  - Reagent
  - Agitator
  - Catalyst

Result:
  - Signature:
    - Reagent & Agitator
    - commutative?
    - ignore m/p/i?
    - non-associative
  - Material:
    - Catalyst.ap( Reagent.m )
  - Purity:
    - Signature.ap( Reagent.p ^ Agitator.p )
  - Instability:
    - Signature.ap( Reagent.i v Agitator.i )


= Spheres =

== Sol (Gold) ==
The Sun tells us of the actual core of a person, the inner self, of that which is of central concern. It also shows us the general vitality and the ability to assert oneself, it describes a general tone of being which colors everything else.
Sign equivalent: Leo

== Luna (Silver) ==
The Moon represents our feelings and emotions, the receptivity, imagination and basic feeling tone of a person. It also has an effect on the sense of rhythm, time and timing, it influences our adaptability to change, our mobility and versatility.
Sign equivalent: Cancer

== Mercury (Mercury) ==
Mercury represents reason, reasonableness (common sense), that which is rational. It stands for the spoken and written word, putting in order, weighing and evaluating, the process of learning and skills.
Sign equivalents: Gemini and Virgo

== Venus (Copper) ==
Venus gives us a sense of beauty, the enjoyment of pleasure, aesthetic awareness, love of harmony, sociability, taking pleasure in relationships and eroticism.
Sign equivalents: Libra and Taurus

== Mars (Iron) ==
Mars represents the energy and drive of a person, their courage, determination, the freedom of spontaneous impulse. It also describes the readiness for action, the way one goes about doing things as well as simple aggression.
Sign equivalent: Aries

== Jupiter (Tin) ==
The search for individual meaning and purpose, optimism, hope and a sense of justice are represented by Jupiter. So also faith, a basic philosophy of life, the striving for spiritual growth and expansion.
Sign equivalent: Sagittarius

== Saturn (Lead) ==
Saturn shows how we experience "reality", where we meet with resistance and discover our limitations. It represents the conscience and moral conviction, the laws and rules which we choose to obey. It also tells us about  our powers of endurance and the ability to concentrate, it lends qualities like earnestness, caution and reserve.
Sign equivalent: Capricorn

== Uranus ==
Uranus stands for intuition, it transmits sudden inspiration and lightning insights. An openness for all that is new, unknown and unusual. A sort of wrong-headed contrariness is also associated with this planet. It is said to be characteristic of astrology as such.
Sign equivalent: Aquarius

== Neptune ==
This planet gives us the supersensory, opens doors to mystical experience and the transcendental. On this level it is hard to discern where perception moves into deception, illusion and false appearances, and so Neptune is associated with all of these, with drugs and all kinds of pseudo-realities.  
Sign equivalent: Pisces

== Pluto ==
Pluto describes how we deal with power, personal and non-personal, be it through suffering the power of others or exercising it ourselves. It describes how we meet the demonic and magical, our regenerative powers and our capacity for radical change and rebirth: the cycles of dying and becoming.  
Sign equivalent: Scorpio


extra elements:
http://mythologian.net/alchemy-symbols-meanings-extended-list-alchemical-symbols/
Arsenic, Platinum, Antimony


Mars       Aries       (Fire) (Cardinal)
Venus(1)   Taurus      (Earth) (Fixed)
Venus(2)   Libra       (Air) (Cardinal)
Mercury(1) Gemini      (Air) (Mutable)
Mercury(2) Virgo       (Earth) (Mutable)
Luna       Cancer      (Water) (Cardinal)
Sol        Leo         (Fire) (Fixed)
Pluto      Scorpius    (Water) (Fixed)
Jupiter    Sagittarius (Fire) (Mutable)
Saturn     Capricornus (Earth) (Cardinal)
Uranus     Aquarius    (Air) (Fixed)
Neptune    Pisces      (Water) (Mutable)

Fire signs: Aries, Leo and Sagittarius
Air signs: Libra, Aquarius, Gemini
Water signs: Cancer, Scorpio, Pisces
Earth signs: Capricorn, Taurus, Virgo

Cardinal signs: Aries, Libra, Cancer, Capricorn
 - Additional minor effect
Fixed signs: Leo, Aquarius, Scorpio, Taurus
 - Bonus to power
Mutable signs: Sagitarius, Gemini, Pisces, Virgo
 - Bonus to instability effects


Effect types:

Mana recovery
Spell override
Debuff mitigation
Mana ID
Buff application
  - swiftness: extra turns

= ITEM EFFECTS =

== MINOR ==
Fast channel (%chance swiftness)
Fast cast (%chance swiftness)
Fast move (%chance swiftness)
Fast craft (%chance swiftness)
Beacon (%chance doubled mana on pickup [element])
Mana capacity +
HP +

== Persistent [Core] ==
Mana collection radius +
Mana collection speed +
Debuff X resistance+ (effective debuff stack size reduced)
HP +++
Flash Craft (crafting is free)
Speed (%chance swiftness)
Insight (%chance ID on pickup)
Spell ID after cast
Feedback (recover mana from greater spells (5+))
[Persistent fire effect]
[Persistent air effect]
[Persistent earth effect]
[Persistent water effect]

== Peristent [Elemental] ==
X ID
XX Affinity (heal from channeling)
XX Channeler (1 channel each turn is free)
XX Flash Caster (casting spells -of specific element- is free)
XX YY Cleansing (channeling X cleanses Y)
Empowered Spell (specific spell is modified) (7 x)
Feedback (regain mana from major spell)

== Immediate [Core] ==
Heal (n points)
Cleanse (n turns)
Apply swiftness (n)
Apply barrier (n)
Identify (n)

== Immediate [Elemental] ==
Gain mana
Blast (radius) [Air]
Freeze (radius) [Water]
Burn (radius) [Fire]
Dig (radius) [Earth]

== Nah ==
Sprinter (gain free action after n steps)
Disable mana displacement

= SPELL CLASSIFICATION =
elementary spell: 1
primitive spell: 2
major spell: 3-4
greater spell: 5+

= TERRAIN EFFECTS =

== PATH ==
== ICE ==
== GRASS ==
== BRAMBLE ==
== EMBERS ==
== BLAZE ==
== INFERNO ==
== ASH ==
== LAVA ==
== POOL ==
== PUDDLE ==
== WATER ==
== STONE WALL ==

= TEMPORARY PLAYER EFFECTS =

== X VULNERABILITY ==
increases applications of X
== MOMENTUM [direction] ==
pushed player in direction. interacts with attempted movement
== ON FIRE ==
== TEMPORAL INSTABILITY ==
== TEMPORAL ANCHOR ==
== SWIFTNESS ==
next action does not end your turn

= UI =

Controls:
Movement: 4 way (arrow keys or ijkl or vi keys?)
Channel: 123(4qwer)
While channeling: movement to cast?
Skip: space
Crafting mode: z

Limits:
Inventory: 6
