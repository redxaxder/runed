
module Shards where

import Extra.Prelude

import Data.Array (replicate, zipWith, zip)
import Data.HeytingAlgebra (implies)
import Permutations.S5 (S5, s5Int)

import Random (Gen)

newtype Shard = Shard
  { signature :: Gen
  , element :: Element
  , purity :: Lattice
  , instability :: Lattice
  }

data Element = Element Valence Metal

derive instance eqElement :: Eq Element
instance showElement :: Show Element where
  show (Element v m) = show v <> show m

data Valence = Bright | Dark

derive instance eqValence :: Eq Valence
instance showValence :: Show Valence where
  show Bright = "+"
  show Dark   = "-"

data Metal
  = Gold     -- ⊙ Sol       --
  | Silver   -- ☽ Luna      --
  | Mercury  -- ☿ Mercury   --
  | Copper   -- ♀ Venus     --
  | Iron     -- ♂ Mars      --
  | Tin      -- ♃ Jupiter   --
  | Lead     -- ♄ Saturn    --
  | Arsenic  -- ⛢ Uranus?   --
  | Antimony -- ♆ Neptune?  --
  | Platinum -- ♇ Pluto?    --


derive instance eqMetal :: Eq Metal

instance showMetal :: Show Metal where
  show Gold     = "⊙"
  show Silver   = "☽"
  show Mercury  = "☿"
  show Copper   = "♀"
  show Iron     = "♂"
  show Tin      = "♃"
  show Lead     = "♄"
  show Arsenic  = "♅"
  show Antimony = "♆"
  show Platinum = "♇"





  -- METAL    --  PLANET     -- ASPECT        -- ALIGNMENT         -- SIGN
  -- Gold     -- ⊙ Sol       -- Fire          -- Fixed             -- Leo
  -- Silver   -- ☽ Luna      -- Water         -- Cardinal          -- Cancer
  -- Mercury  -- ☿ Mercury   -- Air / Earth   -- Mutable           -- Gemini / Virgo
  -- Copper   -- ♀ Venus     -- Earth / Air   -- Fixed / Cardinal  -- Taurus / Libra
  -- Iron     -- ♂ Mars      -- Fire          -- Cardinal          -- Aries
  -- Tin      -- ♃ Jupiter   -- Fire          -- Mutable           -- Sagittarius
  -- Lead     -- ♄ Saturn    -- Earth         -- Cardinal          -- Capricornus
  -- Arsenic  -- ⛢ Uranus?   -- Air           -- Fixed             -- Aquarius
  -- Antimony -- ♆ Neptune?  -- Water         -- Mutable           -- Pisces
  -- Platinum -- ♇ Pluto?    -- Water         -- Fixed             -- Scorpius


-- Order 3 elements of S8
-- 3   -- 4
-- 8   -- 12
-- 11  -- 19
-- 15  -- 20
-- 30  -- 48
-- 38  -- 74
-- 45  -- 99
-- 56  -- 78
-- 59  -- 103
-- 81  -- 104


elementMap :: Array (Tuple Element S5)
elementMap = arr1 <> arr2
  where
  arr1 = zip
    ((\m -> Element Bright m)
      <$> [Gold, Silver, Mercury, Copper, Iron, Tin, Lead, Arsenic, Antimony, Platinum])
    (s5Int
      <$> [    3,     8,      11,     15,   30,  38,   45,      56,       59,       81])
  arr2 = flip map arr1 $ \(Tuple (Element _ m) g) -> Tuple (Element Dark m) (invert g)







type Flag = Boolean
newtype Lattice = Lattice (Array Flag)


instance heytingAlgebraLattice :: HeytingAlgebra Lattice where
  ff = Lattice $ replicate 8 false
  tt = Lattice $ replicate 8 true
  implies (Lattice a) (Lattice b) = Lattice $ zipWith implies a b
  conj (Lattice a) (Lattice b) = Lattice $ zipWith conj a b
  disj (Lattice a) (Lattice b) = Lattice $ zipWith disj a b
  not (Lattice a) = Lattice $ not <$> a

instance showLattice :: Show Lattice where
  show (Lattice l) = show l

derive instance eqLattice :: Eq Lattice

level :: Lattice -> Int
level (Lattice l) = countIf identity l







