
module Permutations.S5 where

import Extra.Prelude

import Partial.Unsafe (unsafePartial)
import Data.Array ((..), unsafeIndex)

import Permutations (L(..))

ix :: forall b. Array b -> Int -> b
ix arr i = unsafePartial $ unsafeIndex arr i

newtype S5 = S5 L

s5Int :: Int -> S5
s5Int x = S5 $ L $ x `mod` 120

instance semigroupS5 :: Semigroup S5 where
  append (S5 (L a)) (S5 (L b)) = s5Table `ix` a `ix` b

instance monoidS5 :: Monoid S5 where
  mempty = S5 mempty

instance groupS5 :: Group S5 where
  invert (S5 (L x)) = s5Inverses `ix` x

instance showS5 :: Show S5 where
  show (S5 x) = show x

s5Table :: Array (Array S5)
s5Table = (\x -> S5 <<< append x <$> s5) <$> s5
  where s5 = L <$> 0 .. 119

s5Inverses :: Array S5
s5Inverses = S5 <<< invert <<< L <$> 0 .. 119

