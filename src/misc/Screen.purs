module Screen where

import Extra.Prelude

{-
import Data.Map (Map)
import Atlas (Position)


type Camera = Vector Int

type Tile = Int

type ScreenElement =
  { position :: Position
  , tile :: Tile
  }

data Chunk = Array (Array ScreenElement)

newtype Screen = Screen (Map (Vector Int) Chunk)

-}

